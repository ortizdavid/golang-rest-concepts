# REST Concepts

Complete REST API Representing most of REST Architecture concepts

## Tools
- Language: **Golang 1.21**
- Database: PostgreSQL
- Routing Library: net/http

## Problem
- Task Management

## REST Concepts
- [x] HTTP Methods
- [ ] HTTP Codes
- [ ] HATEOAS
- [ ] Authentication
- [ ] Autorization

## Other Concepts
- [ ] Error Handling
- [ ] Logging
- [ ] Containerization
- [ ] Monitoring
- [ ] Pagination
- [ ] Documentation

